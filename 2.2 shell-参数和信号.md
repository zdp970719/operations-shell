shell-参数和信号

1、shell参数

 1）几个特殊的变量

​	$0   shell或shell脚本的名字 

​	$1  $2  脚本后所跟的第一个参数，第二个参数，以此类推

​	$*    $@   参数列表

​	$#   参数个数

​	$_    代表上一个命令的最后一个参数

​	$$    代表所在命令的PID

​	$!    代表最后执行的后台命令的PID

2）脚本例子

​	#!/bin/bash 

​	echo -e "\$0=$0 \n"

​	echo -e "\$1=$1 \n"

​	echo -e "\$2=$2 \n"

​	echo -e "\$*=$* \n"

​	echo -e "\$@=$@ \n"

​	echo -e "\$#=$# \n"

​	echo -e "\$$=$$ \n"

​	ls -l ./

​	echo -e "\$_=$_ \n"

3）getopts方式

\#!/bin/bash

function usage()

{

​	echo "usage........ command -b f -d blog "

}

echo "参数个数 $# "

if [ $# -eq "4" ];then

​	while getopts ":b:,:d:" opt;do



​		case $opt in

​		b)

​			echo "backuplevel=$OPTARG"

​			backuplevel=$OPTARG

​			;;

​		d)

​			echo "db=$OPTARG"

​			db=$OPTARG

​			;;

​		?)

​			usage

​			exit 1

​		esac

​	done

​    if [ ! -z $backuplevel ] && [ ! -z $db ] ;then

​		echo "开始备份"

​		if [ $? -eq 0 ];then

​			echo "$backuplevel 级别 backup ${db} success!"

​		fi

​    else

​		echo "有参数未指定"

​		usage

​		exit 1

​	fi	



else

​	usage

​	echo "参数个数不对"

fi

2、exit信号

​	1 ） 如何控制你的脚本退出时的信号 函数中可以用reture  $num 整个脚本退出可以用exit  $num 

​	2）$?表示的是前一个运行的命令或脚本的运行结果，一般情况下，脚本或命令运行正常，则$?为0，不正常情况下，则为非0值

​	3）其实我们可以控制这个信号，传递自定义的值

\#!/bin/bash

function1()

​    {

​        local i=123

​        b=2

​        echo -e "function inner \$i=$i \$b=$b \n"

​        return 99

​        \#exit 19

​        \#后面这个将不会被执行到

​        echo "after return "

​    }

function1

echo "after function \$?=$?"

echo -e "function outer \$i=$i \$b=$b \n"

exit 120

\#后面这个将不会被执行到

echo "after exit"
